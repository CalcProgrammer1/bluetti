BLUETTI EB3A BLUETOOTH REVERSE ENGINEERING
------------------------------------------

The Bluetti EB3A communicates various parameters to its app over Bluetooth Low Energy (BLE).

The EB3A presents 3 services when connected: Gneric Access, Generic Attribute, and a Proprietary FF00 service.

The FF00 service seems to be used for the actual information transfer, the generic services provide standard BLE info like device name.

Attribute FF02 is a writable attribute that the app uses to send commands to the unit.

Attribute FF01 is a notifiable attribute that receives the responses from the unit.

Only one connection allowed at a time

The last two bytes of sent and received packets are a 16-bit CRC-16 big endian checksum

All messages begin with 01 03.  The next byte is the data size for responses and 0 for commands.

COMMAND LIST
------------

Commands are of the format 01 03 XX XX XX YY YY, where XX is the command byte values below and YY is the checksum over the packet inlcuding the 01 03.

10 00 01 - Unknown
------------------

This is the first command sent to the FF02 attribute by the app.

Response is 01 03 02 03 FB F9 37

0A 00 35 - Request Information
------------------------------

This packet gets requested around every 2 seconds.  It returns a long packet containing power supply status information.

Bytes 55 and 56 are DC Input Watts

Bytes 57 and 58 are AC Input Watts

Bytes 59 and 60 are AC Output Watts

Bytes 61 and 62 are DC Output Watts

Byte 70 is battery percentage

Byte 74 is AC input status

Byte 76 is DC input status

Byte 78 is UPS status

Byte 80 is AC output on status

Byte 82 is DC output on status

Sample captures:

01036a45423341000000000000000003fb7d5c366f0207000000000000238f0003232a0003000000000000000000000000000000000000003000000000001c000000010000004700010000000100000000000100000001000000010000000000000000000000000000000000001f9d

01036a45423341000000000000000003fb7d5c366f0207000000000000238f0003232a0003000000000000000000000000000000000000003000000000001c000000010000004700010000000100000000000100000001000000010000000000000000000000000000000000001f9d

01036a45423341000000000000000003fb7d5c366f0207000000000000238f0003232a0003000000000000000000000000000000000000002f00000000001d00000001000000470001000000010000000000010000000100000001000000000000000000000000000000000000ebfc

01036a45423341000000000000000003fb7d5c366f0207000000000000238f0003232a0003000000000000000000000000000000000000002f00000000002300000001000000470001000000010000000000010000000100000001000000000000000000000000000000000000bfbd
