import asyncio
from bleak import *
from os import *

def clear():
     os.system('cls' if os.name=='nt' else 'clear')
     return("   ")

async def main():
    devices = await BleakScanner.discover()
    for d in devices:
        print(d.name)
        
        if(d.name == 'EB3A2230001302876'):
            await run(d.address)
            break

def notification_handler(sender, data):
    clear()

    print("Notification" + ', '.join('{:02x}'.format(x) for x in data))

    ac_in_watts     = (data[57] << 8) + data[58]
    dc_in_watts     = (data[55] << 8) + data[56]
    total_in_watts  = ac_in_watts + dc_in_watts
    ac_out_watts    = (data[59] << 8) + data[60]
    dc_out_watts    = (data[61] << 8) + data[62]
    total_out_watts = ac_out_watts + dc_out_watts

    print("Total Input Watts:  " + str(total_in_watts))
    print("DC Input Watts:     " + str(dc_in_watts))
    print("AC Input Watts:     " + str(ac_in_watts))
    print("Total Output Watts: " + str(total_out_watts))
    print("DC Output Watts:    " + str(dc_out_watts))
    print("AC Output Watts:    " + str(ac_out_watts))

    battery_percent = data[70]

    print("Battery %:          " + str(battery_percent))

    ac_in_on  = data[74]
    dc_in_on  = data[76]

    print("DC Input On:        " + str(dc_in_on))
    print("AC Input On:        " + str(ac_in_on))

    ups_on    = data[78]

    print("UPS On:             " + str(ups_on))
    
    ac_out_on = data[80]
    dc_out_on = data[82]

    print("DC Output On:       " + str(dc_out_on))
    print("AC Output On:       " + str(ac_out_on))

async def run(address):
    async with BleakClient(address) as client:
        x = await client.is_connected()
        notify_uuid = ""
        command_uuid = ""

        print("Connected: {0}".format(x))

        for service in client.services:
            print("Service {0}: {1}".format(service.uuid, service.description))

            if(service.uuid.__contains__('ff00')):

                for characteristic in service.characteristics:
                    print("Characteristic {0}: {1}".format(characteristic.uuid, characteristic.description))

                    if(characteristic.uuid.__contains__('ff01')):
                        print("Notification characteristic received, starting handler")
                        notify_uuid = characteristic.uuid
                        await client.start_notify(notify_uuid, notification_handler)

                    if(characteristic.uuid.__contains__('ff02')):
                        print("Command characteristic received")
                        command_uuid = characteristic.uuid

        await asyncio.sleep(1.0)

        while(1):
            print("Sending command")
            await client.write_gatt_char(command_uuid, bytearray([0x01, 0x03, 0x00, 0x0A, 0x00, 0x35, 0xA5, 0xDF]))
        
            await asyncio.sleep(1.0)

        await asyncio.sleep(1.0)

        await client.stop_notify(notify_uuid)
        await client.disconnect()


asyncio.run(main())
